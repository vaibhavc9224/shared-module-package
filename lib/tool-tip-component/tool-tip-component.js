"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var TooltipComponent = /** @class */ (function () {
    function TooltipComponent() {
    }
    __decorate([
        core_1.Input()
    ], TooltipComponent.prototype, "message", void 0);
    TooltipComponent = __decorate([
        core_1.Component({
            selector: 'cn-tool-tip',
            templateUrl: '../../tool-tip-component.html',
            styleUrls: ['../../tool-tip-component.css']
        })
    ], TooltipComponent);
    return TooltipComponent;
}());
exports.TooltipComponent = TooltipComponent;
//# sourceMappingURL=tool-tip-component.js.map