export interface EnvConfig {
    API?: string;
    ENV?: string;
}
export declare const Config: EnvConfig;
