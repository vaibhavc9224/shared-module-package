export declare class MyGoogleAddress {
    city: string;
    state: string;
    country: string;
    formatted_address: string;
}
