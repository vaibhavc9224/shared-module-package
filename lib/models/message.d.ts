export declare class Message {
    error_msg: any;
    error_code: number;
    custom_message: string;
    isError: boolean;
}
