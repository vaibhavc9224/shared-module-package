/**
 * Created by techprimelab on 9/26/16.
 */
export declare class ErrorInstance {
    err_msg: String;
    err_code: String;
}
