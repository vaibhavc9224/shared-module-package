"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var my_google_address_1 = require("../models/my-google-address");
var MyGoogleDirective = /** @class */ (function () {
    function MyGoogleDirective(el, model) {
        var _this = this;
        this.model = model;
        this.setAddress = new core_1.EventEmitter();
        this.address = new my_google_address_1.MyGoogleAddress();
        this._el = el.nativeElement;
        this.modelValue = this.model;
        var input = this._el;
        var options = {
            types: ['(cities)'],
            componentRestrictions: { country: 'in' }
        };
        this.autocomplete = new google.maps.places.Autocomplete(input, options);
        google.maps.event.addListener(this.autocomplete, 'place_changed', function () {
            _this.place = _this.autocomplete.getPlace();
            _this.invokeEvent(_this.place);
        });
    }
    MyGoogleDirective.prototype.find = function (address_components, query, val) {
        for (var _i = 0, address_components_1 = address_components; _i < address_components_1.length; _i++) {
            var attr = address_components_1[_i];
            for (var _a = 0, _b = attr.types; _a < _b.length; _a++) {
                var type = _b[_a];
                if (type === query) {
                    return val ? attr[val] : attr;
                }
            }
        }
        return null;
    };
    MyGoogleDirective.prototype.findCity = function (address_components) {
        return this.find(address_components, 'locality', 'long_name');
    };
    MyGoogleDirective.prototype.findState = function (address_components) {
        return this.find(address_components, 'administrative_area_level_1', 'long_name');
    };
    MyGoogleDirective.prototype.findCountry = function (address_components) {
        return this.find(address_components, 'country', 'long_name');
    };
    MyGoogleDirective.prototype.invokeEvent = function (place) {
        var city = this.findCity(this.place.address_components);
        var state = this.findState(this.place.address_components);
        var country = this.findCountry(this.place.address_components);
        if (state === undefined) {
            state = city;
        }
        var myaddress = new my_google_address_1.MyGoogleAddress();
        myaddress.city = city;
        myaddress.state = state;
        myaddress.country = country;
        myaddress.formatted_address = this.place.formatted_address;
        this.setAddress.emit(myaddress);
    };
    MyGoogleDirective.prototype.onInputChange = function () {
    };
    __decorate([
        core_1.Output()
    ], MyGoogleDirective.prototype, "setAddress", void 0);
    MyGoogleDirective = __decorate([
        core_1.Directive({
            selector: '[mygoogleplace]',
            providers: [forms_1.NgModel],
            host: {
                '(input)': 'onInputChange()'
            }
        })
    ], MyGoogleDirective);
    return MyGoogleDirective;
}());
exports.MyGoogleDirective = MyGoogleDirective;
//# sourceMappingURL=googleplace.directive.js.map