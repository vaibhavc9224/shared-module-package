import { ElementRef, EventEmitter } from "@angular/core";
import { NgModel } from "@angular/forms";
import { MyGoogleAddress } from "../models/my-google-address";
export declare class MyGoogleDirective {
    private model;
    setAddress: EventEmitter<MyGoogleAddress>;
    address: MyGoogleAddress;
    modelValue: any;
    autocomplete: any;
    private place;
    private _el;
    constructor(el: ElementRef, model: NgModel);
    private find(address_components, query, val);
    findCity(address_components: any[]): any;
    findState(address_components: any[]): any;
    findCountry(address_components: any[]): any;
    invokeEvent(place: Object): void;
    onInputChange(): void;
}
