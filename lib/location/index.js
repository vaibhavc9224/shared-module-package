"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var googleplace_directive_1 = require("./googleplace.directive");
var MyGooglePlaceModule = /** @class */ (function () {
    function MyGooglePlaceModule() {
    }
    MyGooglePlaceModule_1 = MyGooglePlaceModule;
    MyGooglePlaceModule.forRoot = function () {
        return { ngModule: MyGooglePlaceModule_1, providers: [] };
    };
    MyGooglePlaceModule = MyGooglePlaceModule_1 = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, forms_1.ReactiveFormsModule],
            declarations: [googleplace_directive_1.MyGoogleDirective],
            exports: [googleplace_directive_1.MyGoogleDirective],
            providers: []
        })
    ], MyGooglePlaceModule);
    return MyGooglePlaceModule;
    var MyGooglePlaceModule_1;
}());
exports.MyGooglePlaceModule = MyGooglePlaceModule;
//# sourceMappingURL=index.js.map