"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AppSettings = /** @class */ (function () {
    function AppSettings() {
    }
    Object.defineProperty(AppSettings, "API_ENDPOINT", {
        // public static IP = 'app.jobmosis.com';
        // public static IP = '';
        get: function () {
            return this.IP + '/api/';
        },
        enumerable: true,
        configurable: true
    });
    //public static IP = 'http://localhost:8080';
    // public static IP = 'http://ee802b7f.ngrok.io';
    //public static IP = 'http://10.192.33.77:8080';
    AppSettings.IP = 'http://34.208.115.60:3000';
    AppSettings.INITIAL_THEM = 'container-fluid dark-theme';
    AppSettings.LIGHT_THEM = 'container-fluid light-theme';
    AppSettings.IS_SOCIAL_LOGIN_YES = 'YES';
    AppSettings.IS_SOCIAL_LOGIN_NO = 'NO';
    AppSettings.HTTP_CLIENT = 'http://';
    return AppSettings;
}());
exports.AppSettings = AppSettings;
var Messages = /** @class */ (function () {
    function Messages() {
    }
    Messages.MSG_CANDIDATE_NOT_FOUND = "No registered applicant with this name. Avoid using multiple spaces while searching with complete names.";
    Messages.MSG_CANDIDATE_SEARCH_NOT_FOUND = "Applicant's profile does not match with any of your open job profiles.";
    Messages.MSG_CNADIDATE_VISIBILITY_OFF = "The selected applicant profile details are not displayed, since the applicant has marked it as private.";
    Messages.MSG_SUCCESS_LOGIN = 'You are successfully signed in.';
    Messages.MSG_SUCCESS_REGISTRATION = 'Kindly verify your account.';
    Messages.MSG_SUCCESS_CHANGE_MOBILE_NUMBER = 'Mobile number updated successfully.Kindly sign in';
    Messages.MSG_SUCCESS_RESEND_VERIFICATION_CODE = 'New OTP (One Time Password) has been sent to your registered mobile number';
    //public static MSG_SUCCESS_MAIL_VERIFICATION: string = 'Verification e-mail sent successfully to your e-mail account. Kindly proceed by clicking on the link pProvided in your e-mail';
    Messages.MSG_SUCCESS_MAIL_VERIFICATION = 'Verification e-mail sent successfully to your e-mail account.';
    Messages.MSG_SUCCESS_NEWREGISTRATION = 'Registration successful. Mobile number verified. Kindly sign in.';
    Messages.MSG_SUCCESS_RESET_PASSWORD = 'Your password is reset successfully.';
    Messages.MSG_SUCCESS_CHANGE_PASSWORD = 'Your password has been changed successfully.';
    Messages.MSG_SUCCESS_CHANGE_EMAIL = 'Kindly click on the link sent to your new email for email verification.';
    Messages.MSG_SUCCESS_CHANGE_MOBILE = 'Verify your new mobile number by entering OTP sent on your mobile number.';
    Messages.MSG_SUCCESS_FORGOT_PASSWORD = 'Email for password reset has been sent successfully on your registered email id.';
    Messages.MSG_SUCCESS_DASHBOARD_PROFILE = 'Your profile updated successfully.';
    Messages.MSG_SUCCESS_DASHBOARD_PROFILE_PIC = 'Your profile picture updated successfully.';
    Messages.MSG_SUCCESS_ATTACH_DOCUMENT = 'Your document attached successfully.';
    Messages.MSG_SUCCESS_UPLOADED_DOCUMENT = 'Document successfully uploaded.';
    Messages.MSG_SUCCESS_CONTACT = 'Email sent successfully.';
    Messages.MSG_SUCCESS_CHANGE_THEME = 'Theme changed successfully.';
    Messages.MSG_SUCCESS_MAIL_VERIFICATION_RESULT_STATUS = 'Congratulations!';
    Messages.MSG_CHANGE_PASSWORD_SUCCESS_HEADER = 'Password changed successfully';
    Messages.MSG_SUCCESS_MAIL_VERIFICATION_BODY = 'Your account verified successfully.' +
        'You may start using it immediately by clicking on Sign In!';
    Messages.MSG_SUCCESS_FOR_PROFILE_CREATION_STATUS = 'Your profile created successfully.';
    Messages.MSG_SUCCESS_FOR_JOB_POST_STATUS = 'You have successfully posted the new job. You can search for matching candidates for this job through your dashboard.';
    Messages.MSG_ERROR_MAIL_VERIFICATION_BODY = 'Your account verification failed due to invalid access token!';
    Messages.MSG_ERROR_MAIL_VERIFICATION_RESULT_STATUS = 'Sorry.';
    Messages.MSG_ERROR_LOGIN = 'Failed to sign in.';
    Messages.MSG_ERROR_FB_LOGIN = 'Failed to Facebook sign in.';
    Messages.MSG_ERROR_REGISTRATION = 'Failed to register new user.';
    Messages.MSG_ERROR_CHANGE_PASSWORD = 'Failed to change password.';
    Messages.MSG_ERROR_CHANGE_EMAIL = 'Failed to change email.';
    Messages.MSG_ERROR_FORGOT_PASSWORD = 'Failed to reset password.';
    Messages.MSG_ERROR_DASHBOARD_PROFILE = 'Failed to update profile.';
    Messages.MSG_ERROR_CONTACT = 'Failed to send email.';
    Messages.MSG_ERROR_DASHBOARD_PROFILE_PIC = 'Failed to change profile picture.';
    Messages.MSG_ERROR_ATTACH_DOCUMENT = 'Failed to attach document.';
    Messages.MSG_ERROR_CHANGE_THEME = 'Failed to change theme.';
    Messages.MSG_ERROR_TOKEN_SESSION = 'Session has been expired.';
    Messages.MSG_ERROR_NETWORK = 'Internal Server Error.';
    Messages.MSG_ERROR_SERVER_ERROR = 'Server error.';
    Messages.MSG_ERROR_SOMETHING_WRONG = 'Internal Server Error.';
    Messages.MSG_ERROR_IMAGE_TYPE = 'Please try again. Make sure to upload only image file with extensions JPG, JPEG, GIF, PNG.';
    Messages.MSG_ERROR_IMAGE_SIZE = 'Please make sure the image size is less than 5 MB.';
    Messages.MSG_ERROR_DOCUMENT_SIZE = 'Please make sure the document size is less than 5 MB.';
    Messages.MSG_ERROR_FB_AUTH = 'User cancelled login or did not fully authorize.';
    Messages.MSG_ERROR_FB_domain_error = 'The domain of this URL is not included in the app domains.';
    Messages.MSG_WARNING_ON_EDIT_CANDIDATE = 'This section can be updated only after.';
    Messages.MSG_ERROR_VALIDATION_EMAIL_REQUIRED = 'Enter your e-mail address.';
    Messages.MSG_ERROR_VALIDATION_PASSWORD_REQUIRED = 'Enter your password.';
    Messages.MSG_ERROR_VALIDATION_NEWPASSWORD_REQUIRED = 'Enter a new password';
    Messages.MSG_ERROR_VALIDATION_CONFIRMPASSWORD_REQUIRED = 'Confirm your password';
    Messages.MSG_ERROR_VALIDATION_CURRENTPASSWORD_REQUIRED = 'Enter a current password';
    Messages.MSG_ERROR_VALIDATION_FIRSTNAME_REQUIRED = 'This field can\'t be left blank';
    Messages.MSG_ERROR_VALIDATION_LASTNAME_REQUIRED = 'This field can\'t be left blank';
    Messages.MSG_ERROR_VALIDATION_MOBILE_NUMBER_REQUIRED = 'This field can\'t be left blank.';
    Messages.MSG_ERROR_VALIDATION_PIN_REQUIRED = 'Enter your pin code.';
    Messages.MSG_ERROR_VALIDATION_DESCRIPTION_REQUIRED = 'Enter the name of the document you are uploading.';
    Messages.MSG_ERROR_VALIDATION_ABOUT_COMPANY_REQUIRED = 'Give a brief description about your company. This will be seen by candidates as a part of the job profile.';
    Messages.MSG_ERROR_VALIDATION_COMPANYNAME_REQUIRED = 'This field can\'t be left blank.';
    Messages.MSG_ERROR_VALIDATION_OTP_REQUIRED = 'Enter received OTP.';
    Messages.MSG_ERROR_VALIDATION_INVALID_EMAIL_REQUIRED = 'Enter a valid email address.';
    Messages.MSG_ERROR_VALIDATION_INVALID_NAME = 'Enter valid name.';
    Messages.MSG_ERROR_VALIDATION_INVALID_DATA = 'Enter valid data.';
    Messages.MSG_ERROR_VALIDATION_PASSWORD_MISMATCHED = 'Passwords do not match.';
    Messages.MSG_ERROR_VALIDATION_BIRTHYEAR_REQUIRED = 'This field can\'t be left blank.';
    Messages.MSG_ERROR_VALIDATION_LOCATION_REQUIRED = 'This field can\'t be left blank.';
    Messages.MSG_ERROR_VALIDATION_INVALID_LOCATION = 'Enter valid location';
    Messages.MSG_ERROR_VALIDATION_HEADQUARTER_REQUIRED = 'This field can\'t be left blank.';
    Messages.MSG_ERROR_VALIDATION_COMPANYSIZE_REQUIRED = 'This field can\'t be left blank.';
    Messages.MSG_ERROR_VALIDATION_JOBTITLE_REQUIRED = 'This field can\'t be left blank.';
    Messages.MSG_ERROR_VALIDATION_CURRENTCOMPANY_REQUIRED = 'This field can\'t be left blank.';
    Messages.MSG_ERROR_VALIDATION_EDUCATION_REQUIRED = 'This field can\'t be left blank.';
    Messages.MSG_ERROR_VALIDATION_EXPERIENCE_REQUIRED = 'This field can\'t be left blank.';
    Messages.MSG_ERROR_VALIDATION_INDUSTRY_REQUIRED = 'Please select an Industry';
    Messages.MSG_ERROR_VALIDATION_AREAS_WORKED_REQUIRED = 'Select areas you have worked.';
    Messages.MSG_ERROR_VALIDATION_FOR_RECRUITER_AREAS_WORKED_REQUIRED = 'Select areas in which the candidate is expected to work.';
    Messages.MSG_ERROR_VALIDATION_MAX_AREAS_WORKED_CROSSED = 'You have selected maximum work areas. To select a new work area, deselect any of the earlier ones.';
    Messages.MSG_ERROR_VALIDATION_CAPABILITIES_REQUIRED_CANDIDATE = 'Select your capabilities.';
    Messages.MSG_ERROR_VALIDATION_CAPABILITIES_REQUIRED_RECRUITER = 'Select capabilities that are required in the candidate';
    Messages.MSG_ERROR_VALIDATION_MAX_CAPABILITIES_CROSSED = 'You can select maximum 10 capabilities. To select a new capability, deselect any of the earlier selected capability.';
    Messages.MSG_ERROR_VALIDATION_COMPLEXITY_REQUIRED_CANDIDATE = 'Answer this question';
    Messages.MSG_ERROR_VALIDATION_COMPLEXITY_REQUIRED_RECRUITER = 'Do not leave any question blank. If a question is not relevant, select "Not applicable".';
    Messages.MSG_ERROR_VALIDATION_MAX_SKILLS_CROSSED = 'You can select maximum ';
    Messages.MSG_NO_MATCH_FOUND_TEXT = 'This skill is not listed in our skills repository but you can still add this skill by pressing Add button on right.';
    Messages.MSG_ERROR_VALIDATION_KEYSKILLS_REQUIRED = 'Select a value from drop down.';
    Messages.MSG_ERROR_VALIDATION_INDUSTRY_EXPOSURE_REQUIRED = 'This field can\'t be left blank.';
    Messages.MSG_ERROR_VALIDATION_CURRENTSALARY_REQUIRED = 'This field can\'t be left blank.';
    Messages.MSG_ERROR_VALIDATION_RELOCATE_REQUIRED = 'This field can\'t be left blank.';
    Messages.MSG_ERROR_VALIDATION_NOTICEPERIOD_REQUIRED = 'This field can\'t be left blank.';
    Messages.MSG_ERROR_VALIDATION_MAX_WORD_ALLOWED = 'words remaining';
    Messages.MSG_ERROR_VALIDATION_DESIGNATION_REQUIRED = 'This field can\'t be left blank.';
    Messages.MSG_ERROR_VALIDATION_DEGREE_NAME_REQUIRED = 'Degree Name is required.';
    Messages.MSG_ERROR_VALIDATION_UNIVERSITY_NAME_REQUIRED = 'Board/University name is required.';
    Messages.MSG_ERROR_VALIDATION_YEAR_OF_PASSING_REQUIRED = 'Year Of passing is required.';
    Messages.MSG_ERROR_VALIDATION_CERTIFICATION_NAME_REQUIRED = 'Certification name is required.';
    Messages.MSG_ERROR_VALIDATION_CERTIFICATION_AUTHORITY_REQUIRED = 'Authority name is required.';
    Messages.MSG_ERROR_VALIDATION_CERTIFICATION_YEAR_REQUIRED = 'Year Of passing is required.';
    Messages.MSG_ERROR_VALIDATION_AWARD_NAME_REQUIRED = 'Award name is required.';
    Messages.MSG_ERROR_VALIDATION_AWARD_AUTHORITY_REQUIRED = 'Authority name is required.';
    Messages.MSG_ERROR_VALIDATION_AWARD_YEAR_REQUIRED = 'Issued year is required.';
    Messages.MSG_ERROR_VALIDATION_JOB_TITLE_REQUIRED = 'Enter job title.';
    Messages.MSG_ERROR_JOB_TITLE_INVALID_BLANK_SPACE = 'Enter valid job title.';
    Messages.MSG_ERROR_VALIDATION_HIRING_MANAGER_REQUIRED = 'Enter hiring manager name.';
    Messages.MSG_ERROR_VALIDATION_HIRING_DEPARTMENT_REQUIRED = 'Enter hiring department.';
    Messages.MSG_ERROR_VALIDATION_HIRING_COMPANY_REQUIRED = 'Enter hiring company name.';
    Messages.MSG_ERROR_VALIDATION_EDUCATIONAL_QUALIFICATION_REQUIRED = 'Select educational qualification.';
    Messages.MSG_ERROR_VALIDATION_MIN_EXPERIENCE_REQUIRED = 'Select minimum experience expected.';
    Messages.MSG_ERROR_VALIDATION_MAX_EXPERIENCE_REQUIRED = 'Select maximum experience expected.';
    Messages.MSG_ERROR_VALIDATION_EXPERIENCE = 'Select valid Minimum and Maximum experience.';
    Messages.MSG_ERROR_VALIDATION_MIN_SALARY_REQUIRED = 'Select minimum salary offered.';
    Messages.MSG_ERROR_VALIDATION_MAX_SALARY_REQUIRED = 'Select maximum salary offered.';
    Messages.MSG_ERROR_VALIDATION_SALARY = 'Select valid Minimum and Maximum salary band.';
    Messages.MSG_ERROR_VALIDATION_JOINING_PERIOD_REQUIRED = 'Select joining period.';
    Messages.MSG_ERROR_VALIDATION_OTP_MOBILE_NUMBER = 'Please provide a valid mobile number.';
    Messages.MSG_ERROR_VALIDATION_PASSWORD = 'Password Must be Alfa- Numeric having minimum 8 Characters.';
    Messages.MSG_ERROR_VALIDATION_BIRTH_YEAR = "This field can't be left blank.";
    Messages.MSG_ERROR_VALIDATION_PIN_NUMBER = 'Pin code should not be greater than 20 characters.';
    Messages.SUGGESTION_MSG_FOR_RELEVENT_INDUSTRY = 'Based on the profile you have selected, we suggest to search ' +
        'candidate from following industries for matching profiles.\n Unselect if you don\'t want to search candidates from any specific industry.';
    Messages.SUGGESTION_MSG_ABOUT_DOMAIN = 'In addition to<br /> ' + 'this.choosedIndeustry' + ' industry, do you want the ' +
        'candidate to have mandatory experience in any specific Domain? If yes, select such MUST HAVE DOMAINS from below.';
    Messages.MSG_ERROR_VALIDATION_MAX_PROFICIENCIES = ' Key skills. Click the cross sign to deselect existing one and add a new skill.';
    Messages.MSG_ERROR_VALIDATION_EMPLOYMENTHISTORY = 'Provide valid employment start and end date';
    Messages.MSG_LANDING_PAGE = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.' +
        'Nullam sem turpis, sodales eu urna sed, posuere finibus leo finibus.' +
        'Sed et lorem eu mi tincidunt fringilla at non odio.' +
        'Vivamus auctor quam a lobortis tincidunt. Aliquam faucibus nulla lorem, sed imperdiet justo bibendum ac. In' +
        'semper rutrum metus fringilla mollis.';
    Messages.MSG_RESET_MOBILE_NUMBER = 'Enter your new mobile number and we will send you a verification code on mobile' +
        'number you have entered.';
    Messages.MSG_RESET_EMAIL_ADDRESS = 'Enter your new account email address and we will send you a link to reset your email' +
        'address.';
    Messages.MSG_EMAIL_ACTIVATION = 'Your email has been activated. You may start using your account with new email address' +
        'immediately.';
    Messages.MSG_CONTACT_US = 'Please provide the following details and we will get back to you soon.';
    Messages.MSG_YEAR_NO_MATCH_FOUND = 'The year doesn\'t look right. Be sure to use your actual year of birth.';
    Messages.MSG_FORGOT_PASSWORD = 'Enter your account e-mail address and we\'ll help you create a new password.';
    Messages.MSG_READY_FOR_JOB_SEARCH_FOR_FIRST_TIME = 'You are now ready to find your dream job. In a few seconds you will be taken to the job matching dashboard.';
    Messages.MSG_READY_FOR_JOB_SEARCH = 'Your profile edited successfully.You will be taken to the job matching dashboard.';
    Messages.MSG_JOB_POST = 'This job post will now be published. You can see matching candidates for this job in your dashboard view. Proceed?';
    Messages.MSG_CONFIRM_PASSWORD = ' Passwords do not match.';
    Messages.MSG_CHANGE_PASSWORD_SUCCESS = 'Password changed successfully. You can Sign In again with new password by clicking on "YES" button, Please' +
        ' click on "No" button to continue the session.';
    Messages.MSG_VERIFY_USER_1 = 'You are almost done!';
    Messages.MSG_VERIFY_USER_2 = 'We need to verify your mobile number before you can start using the system.';
    Messages.MSG_VERIFY_USER_3 = 'One Time Password(OTP) will be sent on following mobile number.';
    Messages.MSG_VERIFY_USER_4 = 'You are almost done! We need to verify your email id before you can start using the system.';
    Messages.MSG_EMAIL_NOT_MATCH = 'E-mail does not match.';
    Messages.MSG_CHANGE_PASSWORD = 'Your password protects your account so password must be strong.' +
        'Changing your password will sign you out of all your devices, including your phone.' +
        'You will need to enter your new password on all your devices.';
    Messages.MSG_CHANGE_THEME = 'Please click on the below option to change the theme.';
    Messages.MSG_MOBILE_NUMBER_NOT_MATCH = 'Mobile Number does not match.';
    Messages.MSG_MOBILE_NUMBER_Change_SUCCESS = 'Mobile number changed successfully.You can Sign In again by clicking on "yes" button, please click on "No"' +
        'button to continue the session.';
    Messages.MSG_MOBILE_VERIFICATION_TITLE = 'Verify Your Mobile Number';
    Messages.MSG_MOBILE_VERIFICATION_MESSAGE = 'Please enter the verification code sent to your mobile number.';
    Messages.MSG_MOBILE_VERIFICATION_SUCCUSS_HEADING = 'Congratulations!';
    Messages.MSG_MOBILE_VERIFICATION_SUCCUSS_TEXT = 'Registration successful. Kindly Sign In';
    Messages.CONTACT_US_ADDRESS = 'Blog. No. 14, 1st Floor, Electronic Estate, Parvati, Pune-Satara Road, Pune 411009, MH, INDIA.';
    Messages.CONTACT_US_CONTACT_NUMBER_1 = '+91 (20) 2421 8865';
    Messages.CONTACT_US_CONTACT_NUMBER_2 = '+91 98233 18865';
    Messages.CONTACT_US_EMAIL_1 = 'sales@techprimelab.com';
    Messages.CONTACT_US_EMAIL_2 = 'careers@techprimelab.com';
    Messages.MSG_ACTIVATE_USER_1 = 'Congratulations! Welcome To JobMosis.';
    Messages.MSG_ACTIVATE_USER_2 = 'You can now find candidates using the highly accurate, simpler, faster and powerful solution.';
    Messages.MSG_ACTIVATE_USER_3 = 'Your account has been created successfully. Kindly click Sign In.';
    Messages.MSG_COMPANY_DOCUMENTS = 'Please upload relevant company documents to activate your account.';
    Messages.MSG_UPLOAD_FILE = 'Please select a file to upload.';
    Messages.MSG_ABOUT_US_DISCRIPTION = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.' +
        'Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s' +
        'when an unknown printer took a galley of type and scrambled it to make a type specimen book.' +
        'It has survived not only five centuries, but also the leap into electronic typesetting,remaining essentially ' +
        'unchanged. ' +
        'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,' +
        'and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.';
    Messages.BROWSER_ALERT_MSG = 'This application is certified on Google chrome browser. Switch to chrome for better experience.';
    Messages.KEYSKILLS_PLACEHOLDER_DESKTOP = 'E.g. for IT:- Java, C++. for Manufacturing:- Metal Cutting';
    Messages.KEYSKILLS_PLACEHOLDER_MOBILE = 'E.g. for IT:- Java, C++';
    return Messages;
}());
exports.Messages = Messages;
var NavigationRoutes = /** @class */ (function () {
    function NavigationRoutes() {
    }
    NavigationRoutes.APP_REGISTRATION = '/registration';
    NavigationRoutes.APP_FORGOTPASSWORD = '/forgotpassword';
    NavigationRoutes.APP_DASHBOARD = '/dashboard';
    NavigationRoutes.APP_CANDIDATE_DASHBOARD = '/candidate_dashboard';
    NavigationRoutes.APP_ADMIN_DASHBOARD = '/admin_dashboard';
    NavigationRoutes.APP_RECRUITER_DASHBOARD = '/recruiterdashboard';
    NavigationRoutes.APP_LOGIN = '/signin';
    NavigationRoutes.APP_START = '/';
    NavigationRoutes.APP_LANDING = '/landing';
    NavigationRoutes.VERIFY_USER = '/verify_user';
    NavigationRoutes.ACTIVATE_USER = '/activate_user';
    NavigationRoutes.VERIFY_PHONE = '/verify_phone';
    NavigationRoutes.APP_CHANGEEMAIL = '/change_email';
    NavigationRoutes.APP_CREATEPROFILE = '/create_profile';
    NavigationRoutes.APP_PROFILESUMMURY = '/profile_summary';
    NavigationRoutes.APP_JOB_SUMMURY = '/job_summary';
    NavigationRoutes.APP_COMPANYDETAILS = '/company_details';
    return NavigationRoutes;
}());
exports.NavigationRoutes = NavigationRoutes;
var LocalStorage = /** @class */ (function () {
    function LocalStorage() {
    }
    LocalStorage.ACCESS_TOKEN = 'access_token';
    LocalStorage.IS_THEME_SELECTED = 'is_theme_selected';
    LocalStorage.IS_SOCIAL_LOGIN = 'is_social_login';
    LocalStorage.PROFILE_PICTURE = 'profile_picture';
    LocalStorage.ISADMIN = 'is_admin';
    LocalStorage.IS_LOGGED_IN = 'is_user_logged_in';
    LocalStorage.USER_ID = 'user_id';
    LocalStorage.END_USER_ID = 'end_user_id';
    LocalStorage._ID = '_id';
    LocalStorage.IS_CANDIDATE = 'is_candidate';
    LocalStorage.IS_CANDIDATE_FILLED = 'is_candidate_filled';
    LocalStorage.MOBILE_NUMBER = 'mobile_number';
    LocalStorage.COMPANY_NAME = 'company_name';
    LocalStorage.COMPANY_SIZE = 'company_size';
    LocalStorage.FIRST_NAME = 'first_name';
    LocalStorage.LAST_NAME = 'last_name';
    LocalStorage.TEMP_MOBILE = 'temp_mobile';
    LocalStorage.TEMP_EMAIL = 'temp_email';
    LocalStorage.EMAIL_ID = 'email_id';
    LocalStorage.PASSWORD = 'password';
    LocalStorage.MY_THEME = 'my_theme';
    LocalStorage.VERIFY_PHONE_VALUE = 'verify_phone_value';
    LocalStorage.CHANGE_MAIL_VALUE = 'change_mail_value';
    LocalStorage.FROM_CANDIDATE_REGISTRATION = 'from_candidate_registration';
    LocalStorage.AFTER_CANDIDATE_REGISTRATION_FORM = 'after_candidate_registration_form';
    LocalStorage.AFTER_RECRUITER_REGISTRATION_FORM = 'after_recruiter_registration_form';
    LocalStorage.VERIFY_CHANGE_PHONE_VALUE = 'verify_change_phone_value';
    LocalStorage.CURRENT_JOB_POSTED_ID = 'current_job_posted_job_id';
    LocalStorage.POSTED_JOB = 'posted_job';
    LocalStorage.GUIDED_TOUR = 'guided_tour';
    return LocalStorage;
}());
exports.LocalStorage = LocalStorage;
var ValueConstant = /** @class */ (function () {
    function ValueConstant() {
    }
    ValueConstant.MAX_CAPABILITIES = 10;
    ValueConstant.MAX_CAPABILITIES_TO_SHOW = 5;
    ValueConstant.MATCHING_PERCENTAGE = 10;
    ValueConstant.MAX_WORKAREA = 3;
    ValueConstant.MAX_INTERESTEDINDUSTRY = 7;
    ValueConstant.MAX_PROFECIENCES = 25;
    ValueConstant.MAX_MANDATORY_PROFECIENCES = 5;
    ValueConstant.MAX_ADDITIONAL_PROFECIENCES = 5;
    ValueConstant.MAX_YEAR_LIST = 60;
    ValueConstant.MAX_ACADEMIC_YEAR_LIST = 50;
    ValueConstant.SHORT_LISTED_CANDIDATE = 'shortListed';
    ValueConstant.CART_LISTED_CANDIDATE = 'cartListed';
    ValueConstant.REJECTED_LISTED_CANDIDATE = 'rejectedList';
    ValueConstant.PROFILE_COMPARISON_LIST = 'profile_comparison';
    ValueConstant.APPLIED_CANDIDATE = 'applied';
    ValueConstant.BLOCKED_CANDIDATE = 'blocked';
    ValueConstant.MATCHED_CANDIDATE = 'matchedList';
    ValueConstant.VALUE_FOR_CANDIDATES_PERCENT_MATCHING_LOWER_BOUND = 10;
    return ValueConstant;
}());
exports.ValueConstant = ValueConstant;
var API = /** @class */ (function () {
    function API() {
    }
    API.NOTIFICATION = 'notification';
    API.SEND_MAIL = 'sendmail';
    API.SEND_TO_ADMIN_MAIL = 'sendmailtoadmin';
    API.USER_PROFILE = 'users';
    API.UPDATE_USER = 'updateUser';
    API.ALL_USER_PROFILE = 'alluser';
    API.USAGE_DETAIL = 'usageDetails';
    API.CANDIDATE_PROFILE = 'candidate';
    API.CANDIDATE_DETAIL_PROFILE = 'candidateDetails';
    API.RECRUITER_DETAIL_PROFILE = 'recruiterDetails';
    API.RECRUITER_PROFILE = 'recruiter';
    API.PROFESSIONAL_DATA = 'professionaldata';
    API.EMPLOYMENTHISTORY = 'employmentdata';
    API.LOGIN = 'login';
    API.FB_LOGIN = 'fbLogin';
    API.SEARCHED_CANDIDATE = 'searchedcandidate';
    API.SEARCH_CANDIDATE = 'recruiter/candidate';
    API.CHANGE_PASSWORD = 'changepassword';
    API.CHANGE_MOBILE = 'changemobilenumber';
    API.CHANGE_EMAIL = 'changeemailid';
    API.VERIFY_CHANGED_EMAIL = 'verifychangedemailid';
    API.VERIFY_USER = 'verifyAccount';
    API.VERIFY_EMAIL = 'verifyEmail';
    API.GENERATE_OTP = 'generateotp';
    API.VERIFY_OTP = 'verifyotp';
    API.VERIFY_MOBILE = 'verifymobilenumber';
    API.SEND_VERIFICATION_MAIL = 'sendverificationmail';
    API.FORGOT_PASSWORD = 'forgotpassword';
    API.UPDATE_PICTURE = 'updatepicture';
    API.UPLOAD_DOCUMENTS = 'uploaddocuments';
    API.CHANGE_THEME = 'changetheme';
    API.RESET_PASSWORD = 'resetpassword';
    API.GOOGLE_LOGIN = 'googlelogin';
    API.INDUSTRY_PROFILE = 'industryprofile';
    API.INDUSTRY_LIST = 'industry';
    API.REALOCATION = 'realocation';
    API.EDUCATION = 'education';
    API.EXPERIENCE = 'experience';
    API.CURRENTSALARY = 'currentsalary';
    API.NOTICEPERIOD = 'noticeperiod';
    API.INDUSTRYEXPOSURE = 'industryexposure';
    API.PROFICIENCYLIST = 'proficiency';
    API.CAPABILITY_MATRIX_FOR_CANDIDATE = 'capabilitymatrix/candidate';
    API.CAPABILITY_MATRIX_FOR_RECRUITER = 'capabilitymatrix/recruiter/jobProfile';
    API.DOMAINLIST = 'domain';
    API.CAPABILITY_LIST = 'capability';
    API.ROLE_LIST = 'roles';
    API.COMPANY_DETAILS = 'companydetails';
    API.ADDRESS = 'address';
    API.ROLE_TYPE = 'roletype';
    API.JOB_LIST = 'recruiter';
    API.JOB_DETAILS = 'recruiter/jobProfile';
    API.SHORTLIST_CANDIDATE = 'shortlistedcandidate';
    API.CANDIDATE_DETAILS = 'recruiter/jobProfile';
    API.CANDIDATESFROMLISTS = 'recruiter/jobProfile';
    API.RElEVENT_INDUSTRIES = 'releventindustries';
    API.CLONE_JOB = 'job';
    return API;
}());
exports.API = API;
var ImagePath = /** @class */ (function () {
    function ImagePath() {
    }
    ImagePath.FAV_ICON = './assets/framework/images/logo/favicon.ico';
    ImagePath.BODY_BACKGROUND = './assets/framework/images/page_background/page-bg.png';
    ImagePath.MY_COLOR_LOGO = './assets/framework/images/logo/logo-color.png';
    ImagePath.MY_WHITE_LOGO = './assets/framework/images/logo/job-mosis-logo.png';
    ImagePath.MOBILE_WHITE_LOGO = './assets/framework/images/logo/jobmosis-mobile-logo.png';
    ImagePath.FACEBOOK_ICON = './assets/framework/images/footer/fb.svg';
    ImagePath.GOOGLE_ICON = './assets/framework/images/footer/google-plus.svg';
    ImagePath.LINKEDIN_ICON = './assets/framework/images/footer/linked-in.svg';
    ImagePath.PROFILE_IMG_ICON = './assets/framework/images/dashboard/default-profile.png';
    ImagePath.COMPANY_LOGO_IMG_ICON = './assets/framework/images/dashboard/default-company-logo.png';
    ImagePath.EMAIL_ICON = './assets/framework/images/icons/e-mail.svg';
    ImagePath.EMAIL_ICON_GREY = './assets/framework/images/icons/e-mail-grey.svg';
    ImagePath.NEW_EMAIL_ICON = './assets/framework/images/icons/new-e-mail.svg';
    ImagePath.NEW_EMAIL_ICON_GREY = './assets/framework/images/icons/new-e-mail-grey.svg';
    ImagePath.CONFIRM_EMAIL_ICON = './assets/framework/images/icons/confirm-e-mail.svg';
    ImagePath.CONFIRM_EMAIL_ICON_GREY = './assets/framework/images/icons/confirm-e-mail-grey.svg';
    ImagePath.PASSWORD_ICON = './assets/framework/images/icons/password.svg';
    ImagePath.PASSWORD_ICON_GREY = './assets/framework/images/icons/password-grey.svg';
    ImagePath.NEW_PASSWORD_ICON = './assets/framework/images/icons/new-password.svg';
    ImagePath.NEW_PASSWORD_ICON_GREY = './assets/framework/images/icons/new-password-grey.svg';
    ImagePath.CONFIRM_PASSWORD_ICON = './assets/framework/images/icons/confirm-password.svg';
    ImagePath.CONFIRM_PASSWORD_ICON_GREY = './assets/framework/images/icons/confirm-password-grey.svg';
    ImagePath.MOBILE_ICON = './assets/framework/images/icons/mobile.svg';
    ImagePath.MOBILE_ICON_GREY = './assets/framework/images/icons/mobile-grey.svg';
    ImagePath.NEW_MOBILE_ICON = './assets/framework/images/icons/new-mobile.svg';
    ImagePath.NEW_MOBILE_ICON_GREY = './assets/framework/images/icons/new-mobile-grey.svg';
    ImagePath.CONFIRM_MOBILE_ICON = './assets/framework/images/icons/confirm-mobile.svg';
    ImagePath.CONFIRM_MOBILE_ICON_GREY = './assets/framework/images/icons/confirm-mobile-grey.svg';
    ImagePath.FIRST_NAME_ICON = './assets/framework/images/icons/first-name.svg';
    ImagePath.FIRST_NAME_ICON_GREY = './assets/framework/images/icons/first-name-grey.svg';
    ImagePath.LAST_NAME_ICON = './assets/framework/images/icons/last-name.svg';
    ImagePath.LAST_NAME_ICON_GREY = './assets/framework/images/icons/last-name-grey.svg';
    //guided tour images for desktop
    ImagePath.BASE_ASSETS_PATH_DESKTOP = './assets/c-next/guided-tour/tour-for-desktop/';
    ImagePath.CANDIDATE_OERLAY_SCREENS_CAPABILITIES = 'candidate_overlay-screens-capabilities.jpg';
    ImagePath.CANDIDATE_OERLAY_SCREENS_COMPLEXITIES = 'candidate_overlay-screens-complexities.jpg';
    ImagePath.CANDIDATE_OERLAY_SCREENS_DASHBOARD = 'candidate_overlay-screens-dashboard.jpg';
    ImagePath.CANDIDATE_OERLAY_SCREENS_EMPLOYMENT_HISTORY = 'candidate_overlay-screens-emloyment-history.jpg';
    ImagePath.CANDIDATE_OERLAY_SCREENS_KEY_SKILLS = 'candidate_overlay-screens-key-skills.jpg';
    ImagePath.CANDIDATE_OERLAY_SCREENS_PROFILE = 'candidate_overlay-screens-profile.jpg';
    ImagePath.CANDIDATE_OVERLAY_SCREENS_BASIC_INFO = 'candidate_overlay-screens-basic-info.jpg';
    ImagePath.CANDIDATE_OERLAY_SCREENS_STACK_VIEW = 'candidate_overlay-screens-stack-view.jpg';
    return ImagePath;
}());
exports.ImagePath = ImagePath;
var ProjectAsset = /** @class */ (function () {
    function ProjectAsset() {
    }
    ProjectAsset._year = new Date();
    ProjectAsset.currentYear = ProjectAsset._year.getFullYear();
    ProjectAsset.APP_NAME = 'JobMosis';
    ProjectAsset.TAG_LINE = 'The Awesome Web Experience';
    ProjectAsset.UNDER_LICENECE = '© ' + ProjectAsset.currentYear + ' www.jobmosis.com';
    return ProjectAsset;
}());
exports.ProjectAsset = ProjectAsset;
var Tooltip = /** @class */ (function () {
    function Tooltip() {
    }
    Tooltip.ACADEMIC_DETAIL_TOOLTIP = 'An individual must provide latest qualification details first.';
    Tooltip.AWARDS_TOOLTIP = 'Award message.';
    Tooltip.BASIC_JOB_INFORMATION_TOOLTIP_1 = 'This job name would be displayed in the posting.';
    Tooltip.BASIC_JOB_INFORMATION_TOOLTIP_2 = 'Name of the manager who has given the requirement for this job.';
    Tooltip.BASIC_JOB_INFORMATION_TOOLTIP_3 = 'Name of the department for which the candidate is being hired.';
    Tooltip.BASIC_JOB_INFORMATION_TOOLTIP_4 = 'Choose from dropdown.';
    Tooltip.BASIC_JOB_INFORMATION_TOOLTIP_5 = 'The target salary that you wish to offer for the job.';
    Tooltip.BASIC_JOB_INFORMATION_TOOLTIP_6 = 'How much lead time are you willing to provide to the candidate for joining.';
    Tooltip.BASIC_JOB_INFORMATION_TOOLTIP_7 = 'The location where the candidate will be required to work.';
    Tooltip.BASIC_JOB_INFORMATION_TOOLTIP_8 = 'The industry for which you are hiring.';
    Tooltip.EMPTY_CANDIDATE_DASHBOARD_MESSAGE = 'Currently there are no jobs matching to your profile.' +
        'As the current jobs posted by recruiters demand a different set of capabilities than what you possess.' +
        'This dashboard shows job postings that match your capability profile.' +
        'It is recommended that you keep visiting this page frequently to see the best matching jobs.';
    Tooltip.APPLIED_JOB_MESSAGE = 'Presently you have not applied for any job.';
    Tooltip.NOT_INTRESTED_JOB_MESSAGE = 'Currently you have not marked any jobs as "Not Interested".';
    Tooltip.PROFILE_INFO_VISIBILIT_SET_TO_NO = 'If "No", your profile will not be visible to recruiter.' +
        'If you are on the lookout of job change, it is recommended to keep this setting to "Yes". You can change this setting later.';
    Tooltip.PROFILE_INFO_VISIBILIT_SET_TO_YES = 'If "Yes", your profile will be available in employer search.';
    Tooltip.CANDIDATE_CAPABILITY_TOOLTIP_1 = 'Select those capabilities that describe your current strength. These capabilities would define you in the eyes of the recruiter and help you align with the best suitable job.';
    Tooltip.CANDIDATE_CAPABILITY_TOOLTIP_2 = 'If there are capabilities that you have developed in past but are no more relevent, you should not select such capabilites as this would dilute the matching and alignment with the best job opportunity.';
    Tooltip.RECRUITER_CAPABILITY_TOOLTIP = 'These capabilities would form the core of the job profile. ' +
        'In next section, you would get to define these capabilities in detail.';
    Tooltip.CERTIFICATE_TOOLTIP = 'Certification/Accreditation Message';
    Tooltip.COMPETENCIES_AND_RESPONSIBILITIES_TOOLTIP_1 = 'Additional Information';
    Tooltip.COMPETENCIES_AND_RESPONSIBILITIES_TOOLTIP_2 = 'You can use this field to describe specific aspects of the job profile that will help the candidate to understand your expectations better.';
    Tooltip.COMPLEXITIES_CANDIDATE_TOOLTIP_1 = 'This section provides a list of complexity scenarios for your selected capabilities.' +
        'If more than one options are applicable to you, choose the option where you can demonstrate a higher level of expertise.';
    Tooltip.COMPLEXITIES_CANDIDATE_TOOLTIP_2 = 'If a scenario was applicable to you in past but is no more relevant to you, avoid choosing such scenarios.In such cases, choose "Not Applicable".';
    Tooltip.COMPLEXITIES_RECRUITER_TOOLTIP_1 = 'This section provides a list of complexity scenarios for selected capabilities.' +
        'For each scenario, select the most appropriate level that candidate is required to handle.';
    Tooltip.COMPLEXITIES_RECRUITER_TOOLTIP_2 = 'For scenarios that are not relevant to your job profile, choose "Not Applicable".';
    Tooltip.EMPLOYMENT_HISTORY_TOOLTIP = 'An individual may be exposed to multiple industries during his professional life.';
    Tooltip.INDUSTRY_EXPERIENCE_CANDIDATE_TOOLTIP_1 = 'An individual may be exposed to multiple industries during their professional life. ' +
        'At times, organisations need individuals who have cross industry expertise.';
    Tooltip.INDUSTRY_EXPERIENCE_CANDIDATE_TOOLTIP_2 = 'Select such industries where you can claim a reasonable exposure.';
    Tooltip.INDUSTRY_EXPERIENCE_RECRUITER_TOOLTIP = 'If you wish the candidate to have exposure to any industry besides his core industry, please select such additional industries.';
    Tooltip.INDUSTRY_LIST_TOOLTIP_1 = 'Enter the industry from which you wish to hire the candidate. This Industry forms the core of your Job Profile posting. In next sections, you shall be shown questions and parameters that are relevant to this Industry.';
    Tooltip.INDUSTRY_LIST_TOOLTIP_2 = 'If you wish the candidate to have worked in multiple Industries, choose the one that is most relevent as on date. You shall get option to include additional industries in Relevant Industry section.';
    Tooltip.JOB_PROFICIENCIES_TOOLTIP_1 = 'Enter keywords for specialization in Technologies, Products, Tools, Domains etc. E.g Java, Oracle, SAP, Cognos, AWS, Agile, DevOps, CMM, Telecom Billing, Retail Banking etc.';
    Tooltip.JOB_PROFICIENCIES_TOOLTIP_2 = 'Use the Top 5 "Must Have" keywords to describe the mandatory skills. You can provide additional 5 keywords that are "Nice to Have".';
    Tooltip.MORE_ABOUT_MYSELF_TOOLTIP = 'Please mention additional details about your personal and professional journey that would help the recruiter to know you better.';
    Tooltip.PROFESSIONAL_DATA_TOOLTIP_1 = 'Please mention your current salary (CTC).';
    Tooltip.PROFESSIONAL_DATA_TOOLTIP_2 = 'Select if you are open to relocate from your current location as per job demand.';
    Tooltip.PROFESSIONAL_DATA_TOOLTIP_3 = 'Mention the notice period you have to serve before you can take up new job.';
    Tooltip.PROFICIENCIES_TOOLTIP_1 = 'Enter all key words that describe your area of expertise or specialization.';
    Tooltip.PROFICIENCIES_TOOLTIP_2 = 'Ensure that you cover all relevant aspects of Technologies, Products, Methodologies, Models,' +
        'Processes, Tools, Domain expertise and any additional key words that describe your work.';
    Tooltip.PROFICIENCIES_TOOLTIP_3 = 'Selecting too many Key Skills would dilute the matching and alignment with the ' +
        'best job opportunity. Hence you should select maximum 25 Key Skills.';
    Tooltip.PROFILE_DESCRIPTION_TOOLTIP_1 = 'Enter your current or latest job title.';
    Tooltip.PROFILE_DESCRIPTION_TOOLTIP_2 = 'A profile photo helps the recruiter to associate a face to the name.';
    Tooltip.PROFILE_DESCRIPTION_TOOLTIP_3 = 'Provide your current or latest company name.Freshers should mention "Fresher" as their company name.';
    Tooltip.RECRUITER_ENTRY_MESSAGE = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industrys' +
        ' standard dummy text ever since the 1500s,when an unknown printer took a galley of type and scrambled it to make' +
        ' a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting,' +
        'remaining essentially unchanged.';
    Tooltip.RECRUITER_DASHBOARD_MESSAGE = 'Welcome To Dashboard!';
    Tooltip.RELEVENT_INDUSTRY_LIST_TOOLTIP = 'Based on the profile you have selected, You can select industries to get more candidates with matching profiles.';
    Tooltip.SAVE_ROLES_MESSAGE = 'Saving role details. Once saved, you cannot change it for 3 months.';
    Tooltip.AREA_OF_WORK_TOOLTIP_1 = 'Select those areas of work that best describe your current focus.';
    Tooltip.AREA_OF_WORK_TOOLTIP_2 = 'If there are areas that you have worked in past but are no more relevent, you should not select such areas as they may fetch jobs that are no more relevant to you.';
    Tooltip.RECRUITER_AREA_OF_WORK_TOOLTIP = 'Select those areas in which the candidate is expected to work. You can select maximum 3 areas of work for a job profile in order to make your search more relevant.';
    Tooltip.EMPTY_LIST_MESSAGE = 'Currently there are no candidates matching to your job posting.' +
        'This is because the currently available candidates possess different set of capabilities than' +
        'what your job expects. This dashboard shows candidates that have best matches with your desired' +
        'capability profile. It is recommended that you keep visiting this page frequently to see the best matching candidates.';
    Tooltip.EMPTY_CART_MESSAGE = 'You have not added any candidates to the Cart for this job posting.';
    Tooltip.EMPTY_REJECTED_LIST_MESSAGE = 'There are no candidates rejected by you for this job posting.';
    Tooltip.CAPABILITY_COMPARE_ABOVE_MATCH = 'Candidate capabilities higher than desired';
    Tooltip.CAPABILITY_COMPARE_EXACT_MATCH = 'Candidate capabilities with exact match';
    Tooltip.CAPABILITY_COMPARE_BELOW_MATCH = 'Candidate capabilities slightly less than desired';
    Tooltip.CAPABILITY_COMPARE_MISSING_MATCH = 'Large mismatch of capabilities';
    Tooltip.COMPANY_DETAILS_TOOLTIP = 'Company Details Message';
    return Tooltip;
}());
exports.Tooltip = Tooltip;
var Headings = /** @class */ (function () {
    function Headings() {
    }
    Headings.ACADAMIC_DETAILS = 'Academic Details (Optional)';
    Headings.AWARDS = 'Awards (Optional)';
    Headings.JOB_DISCRIPTION = 'Job Description';
    Headings.HIDE_COMPANY_NAME = 'Hide company Name from applicant';
    Headings.GOT_IT = 'OK, Got it';
    Headings.CAPABILITIES_FOR_CANDIDATE = 'Select those capabilities that describe your current strength.';
    Headings.CAPABILITIES_FOR_RECRUITER = 'Select core capabilities that are required in the candidate.';
    Headings.CERTIFICATE_ACCREDITATION = 'Certification/Accreditation (Optional)';
    Headings.ADDITIONAL_INFORMATION = 'Additional information about the job';
    Headings.OPTIONAL = '(Optional)';
    Headings.CAPABITITIES_HEADING = 'Capabilities';
    Headings.EMPLOYMENT_HISTORY = 'Employment History';
    Headings.ADDITIONAL_DOMAIN_EXPOSURE = 'Additional domain exposure';
    Headings.INDUSTRY_FOR_CANDIDATE = 'Select your Industry (Any One)';
    Headings.INDUSTRY_FOR_RECRUITER = 'Select industry in which candidate is expected to work (Any One)';
    Headings.JOB_PROFICIENCIES = 'Keywords that describe candidate\'s area of expertise';
    Headings.MANDATORY_PROFICIENCIES = 'Mandatory Key Skills';
    Headings.ADDITIONAL_PROFICIENCIES = 'Additional Key Skills';
    Headings.ABOUT_MYSELF = 'About Myself';
    Headings.SUPPLIMENTARY_CAPABILITIES = 'Supplimentary Capabilities';
    Headings.ADDITIONAL_INFORMATION_TEXT = 'Additional Information';
    Headings.KEY_SKILLS = 'Key Skills';
    Headings.CHANGE_PASSWORD = 'Change Password';
    Headings.ACCOUNT_DETAILS_HEADING = 'Account Details';
    Headings.CHANGE_EMAIL_HEADING = 'Change your Email';
    Headings.CHANGE_MOBILE_NUMBER_HEADING = 'Change Your Mobile Number';
    Headings.RESET_PASSWORD_HEADING = 'RESET PASSWORD';
    return Headings;
}());
exports.Headings = Headings;
var Label = /** @class */ (function () {
    function Label() {
    }
    Label.CURRENT_PASSWORD_LABEL = 'Current Password';
    Label.NEW_PASSWORD_LABEL = 'New Password';
    Label.CONFIRM_PASSWORD_LABEL = 'Confirm Password';
    Label.FIRST_NAME_LABEL = 'First Name';
    Label.LAST_NAME_LABEL = 'Last Name';
    Label.EMAIL_FIELD_LABEL = 'Email';
    Label.CONTACT_FIELD_LABEL = 'Contact';
    Label.SAVE_PROFILE_LABEL = 'Save Profile';
    Label.RESET_PASSWORD_MESSAGE = 'Please set new password for your';
    return Label;
}());
exports.Label = Label;
var Button = /** @class */ (function () {
    function Button() {
    }
    Button.CHANGE_PASSWORD_BUTTON = 'Change Password';
    Button.RESET_PASSWORD_BUTTON = 'RESET PASSWORD';
    return Button;
}());
exports.Button = Button;
//# sourceMappingURL=constants.js.map