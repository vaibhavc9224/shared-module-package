"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var loaders_service_1 = require("./loaders.service");
var LoaderComponent = /** @class */ (function () {
    function LoaderComponent(loaderService) {
        var _this = this;
        this.loaderService = loaderService;
        loaderService.status.subscribe(function (status) {
            _this.status = status;
        });
    }
    LoaderComponent = __decorate([
        core_1.Component({
            selector: 'cn-app-loader',
            template: "\n    <div *ngIf='status' class='loader-container'>\n      <img src=\"assets/c-next/loader/main-loading.svg\" height=\"20\" width=\"160\" style=\"margin-top: 400px;\"/>\n    </div>"
        }),
        __param(0, core_1.Inject(loaders_service_1.LoaderService))
    ], LoaderComponent);
    return LoaderComponent;
}());
exports.LoaderComponent = LoaderComponent;
//# sourceMappingURL=loader.component.js.map