"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* tslint:disable:no-unused-variable */
var testing_1 = require("@angular/core/testing");
var loaders_service_1 = require("./loaders.service");
describe('LoaderService', function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [loaders_service_1.LoaderService]
        });
    });
    it('should ...', testing_1.inject([loaders_service_1.LoaderService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=loader.service.spec.js.map