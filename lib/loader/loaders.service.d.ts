import { Subject } from "rxjs/Subject";
export declare class LoaderService {
    status: Subject<boolean>;
    start(): void;
    stop(): void;
}
