"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = require("../constants");
var ValidationService = /** @class */ (function () {
    function ValidationService() {
    }
    ValidationService.getValidatorErrorMessage = function (validatorName, validatorValue) {
        var config = {
            'required': 'Required',
            'requiredEmail': constants_1.Messages.MSG_ERROR_VALIDATION_EMAIL_REQUIRED,
            'requiredPassword': constants_1.Messages.MSG_ERROR_VALIDATION_PASSWORD_REQUIRED,
            'requiredNewPassword': constants_1.Messages.MSG_ERROR_VALIDATION_NEWPASSWORD_REQUIRED,
            'requiredConfirmPassword': constants_1.Messages.MSG_ERROR_VALIDATION_CONFIRMPASSWORD_REQUIRED,
            'requiredCurrentPassword': constants_1.Messages.MSG_ERROR_VALIDATION_CURRENTPASSWORD_REQUIRED,
            'requiredFirstName': constants_1.Messages.MSG_ERROR_VALIDATION_FIRSTNAME_REQUIRED,
            'requiredLastName': constants_1.Messages.MSG_ERROR_VALIDATION_LASTNAME_REQUIRED,
            'requiredMobileNumber': constants_1.Messages.MSG_ERROR_VALIDATION_MOBILE_NUMBER_REQUIRED,
            'requiredPin': constants_1.Messages.MSG_ERROR_VALIDATION_PIN_REQUIRED,
            'requiredDescription': constants_1.Messages.MSG_ERROR_VALIDATION_DESCRIPTION_REQUIRED,
            'requiredCompanyDescription': constants_1.Messages.MSG_ERROR_VALIDATION_ABOUT_COMPANY_REQUIRED,
            'requiredCompanyName': constants_1.Messages.MSG_ERROR_VALIDATION_COMPANYNAME_REQUIRED,
            'requiredOtp': constants_1.Messages.MSG_ERROR_VALIDATION_OTP_REQUIRED,
            'invalidEmailAddress': constants_1.Messages.MSG_ERROR_VALIDATION_INVALID_EMAIL_REQUIRED,
            'invalidName': constants_1.Messages.MSG_ERROR_VALIDATION_INVALID_NAME,
            'containsWhiteSpace': constants_1.Messages.MSG_ERROR_VALIDATION_INVALID_DATA,
            'invalidPassword': constants_1.Messages.MSG_ERROR_VALIDATION_PASSWORD,
            'invalidMobile': constants_1.Messages.MSG_ERROR_VALIDATION_OTP_MOBILE_NUMBER,
            'invalidBirthYear': constants_1.Messages.MSG_ERROR_VALIDATION_BIRTH_YEAR,
            'invalidPin': constants_1.Messages.MSG_ERROR_VALIDATION_PIN_NUMBER,
            'maxlength': "Maximum " + validatorValue.requiredLength + " characters",
            'minlength': "Minimum " + validatorValue.requiredLength + " characters"
        };
        return config[validatorName];
    };
    ValidationService.emailValidator = function (control) {
        if (control.value) {
            if (control.value.match(/[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/)) {
                return null;
            }
            else {
                return { 'invalidEmailAddress': true };
            }
        }
        return null;
    };
    ValidationService.requireEmailValidator = function (control) {
        if (control.value === '' || control.value === undefined) {
            return { 'requiredEmail': true };
        }
        else {
            return null;
        }
    };
    ValidationService.requireFirstNameValidator = function (control) {
        if (control.value === '' || control.value === undefined) {
            return { 'requiredFirstName': true };
        }
        else {
            return null;
        }
    };
    ValidationService.nameValidator = function (control) {
        if (control.value.match(/^[a-zA-Z](?:[a-zA-Z ]*[a-zA-Z])?$/)) {
            return null;
        }
        else {
            return { 'invalidName': true };
        }
    };
    ValidationService.requireCompanyNameValidator = function (control) {
        if (control.value === '' || control.value === undefined) {
            return { 'requiredCompanyName': true };
        }
        else {
            return null;
        }
    };
    ValidationService.noWhiteSpaceValidator = function (control) {
        var isWhitespace = (control.value).trim().length === 0;
        if (isWhitespace) {
            return { 'containsWhiteSpace': true };
        }
        else {
            return null;
        }
    };
    ValidationService.requireCompanyDescriptionValidator = function (control) {
        if (control.value === '' || control.value === undefined) {
            return { 'requiredCompanyDescription': true };
        }
        else {
            return null;
        }
    };
    ValidationService.requireLastNameValidator = function (control) {
        if (control.value === '' || control.value === undefined) {
            return { 'requiredLastName': true };
        }
        else {
            return null;
        }
    };
    ValidationService.requireMobileNumberValidator = function (control) {
        if (control.value === '' || control.value === undefined) {
            return { 'requiredMobileNumber': true };
        }
        else {
            return null;
        }
    };
    ValidationService.passwordValidator = function (control) {
        if (control.value.match(/(?=.*\d)(?=.*[a-zA-Z]).{8,}/)) {
            return null;
        }
        else {
            return { 'invalidPassword': true };
        }
    };
    ValidationService.requirePasswordValidator = function (control) {
        if (control.value === '' || control.value === undefined) {
            return { 'requiredPassword': true };
        }
        else {
            return null;
        }
    };
    ValidationService.requireNewPasswordValidator = function (control) {
        if (control.value === '' || control.value === undefined) {
            return { 'requiredNewPassword': true };
        }
        else {
            return null;
        }
    };
    ValidationService.requireCurrentPasswordValidator = function (control) {
        if (control.value === '' || control.value === undefined) {
            return { 'requiredCurrentPassword': true };
        }
        else {
            return null;
        }
    };
    ValidationService.requireOtpValidator = function (control) {
        if (control.value === '' || control.value === undefined) {
            return { 'requiredOtp': true };
        }
        else {
            return null;
        }
    };
    ValidationService.requireConfirmPasswordValidator = function (control) {
        if (control.value === '' || control.value === undefined) {
            return { 'requiredConfirmPassword': true };
        }
        else {
            return null;
        }
    };
    ValidationService.requirePinValidator = function (control) {
        if (control.value === '' || control.value === undefined) {
            return { 'requiredPin': true };
        }
        else {
            return null;
        }
    };
    ValidationService.requireDescriptionValidator = function (control) {
        if (control.value === '' || control.value === undefined) {
            return { 'requiredDescription': true };
        }
        else {
            return null;
        }
    };
    ValidationService.mobileNumberValidator = function (control) {
        var mobileNumber = control.value;
        var count = 0;
        while (mobileNumber > 1) {
            mobileNumber = (mobileNumber / 10);
            count += 1;
        }
        if (count === 10) {
            return null;
        }
        else {
            return { 'invalidMobile': true };
        }
    };
    ValidationService.birthYearValidator = function (control) {
        var birthYear = control.value;
        var count = 0;
        var isValid = false;
        var currentDate = new Date();
        var year = currentDate.getUTCFullYear() - 18;
        if (birthYear > year - 60 && birthYear <= year) {
            isValid = true;
        }
        while (birthYear > 1) {
            birthYear = (birthYear / 10);
            count += 1;
        }
        if (count === 4 && isValid === true) {
            return null;
        }
        else {
            return { 'invalidBirthYear': true };
        }
    };
    ValidationService.pinValidator = function (control) {
        var pin = control.value.length;
        if (pin <= 20) {
            return null;
        }
        else {
            return { 'invalidPin': true };
        }
    };
    return ValidationService;
}());
exports.ValidationService = ValidationService;
//# sourceMappingURL=validation.service.js.map