export declare class ValidationService {
    static getValidatorErrorMessage(validatorName: string, validatorValue?: any): any;
    static emailValidator(control: any): {
        'invalidEmailAddress': boolean;
    };
    static requireEmailValidator(control: any): {
        'requiredEmail': boolean;
    };
    static requireFirstNameValidator(control: any): {
        'requiredFirstName': boolean;
    };
    static nameValidator(control: any): {
        'invalidName': boolean;
    };
    static requireCompanyNameValidator(control: any): {
        'requiredCompanyName': boolean;
    };
    static noWhiteSpaceValidator(control: any): {
        'containsWhiteSpace': boolean;
    };
    static requireCompanyDescriptionValidator(control: any): {
        'requiredCompanyDescription': boolean;
    };
    static requireLastNameValidator(control: any): {
        'requiredLastName': boolean;
    };
    static requireMobileNumberValidator(control: any): {
        'requiredMobileNumber': boolean;
    };
    static passwordValidator(control: any): {
        'invalidPassword': boolean;
    };
    static requirePasswordValidator(control: any): {
        'requiredPassword': boolean;
    };
    static requireNewPasswordValidator(control: any): {
        'requiredNewPassword': boolean;
    };
    static requireCurrentPasswordValidator(control: any): {
        'requiredCurrentPassword': boolean;
    };
    static requireOtpValidator(control: any): {
        'requiredOtp': boolean;
    };
    static requireConfirmPasswordValidator(control: any): {
        'requiredConfirmPassword': boolean;
    };
    static requirePinValidator(control: any): {
        'requiredPin': boolean;
    };
    static requireDescriptionValidator(control: any): {
        'requiredDescription': boolean;
    };
    static mobileNumberValidator(control: any): {
        'invalidMobile': boolean;
    };
    static birthYearValidator(control: any): {
        'invalidBirthYear': boolean;
    };
    static pinValidator(control: any): {
        'invalidPin': boolean;
    };
}
