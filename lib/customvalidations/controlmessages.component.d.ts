import { FormControl } from "@angular/forms";
export declare class ControlMessagesComponent {
    control: FormControl;
    submitStatus: boolean;
    readonly errorMessage: any;
}
