import { Subject } from "rxjs/Subject";
import { Message } from "../models/message";
import { Observable } from "rxjs/Observable";
export declare class MessageService {
    MessageSource: Subject<Message>;
    messageObservable$: Observable<Message>;
    message(message: Message): void;
}
