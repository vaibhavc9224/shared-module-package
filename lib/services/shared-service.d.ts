export declare class SharedService {
    isChrome: boolean;
    isToasterVisible: boolean;
    constructor();
    detectIE(): any;
    setToasterVisiblity(isToasterVisible: boolean): void;
    getToasterVisiblity(): boolean;
    getUserBrowser(): boolean;
}
