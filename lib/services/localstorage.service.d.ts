export declare class LocalStorageService {
    static ACCESS_TOKEN: string;
    static getLocalValue(key: any): string;
    static removeLocalValue(key: any): void;
    static setLocalValue(key: any, value: any): void;
}
