import { Subject } from "rxjs/Subject";
import { Observable } from "rxjs/Observable";
export declare class ThemeChangeService {
    themeSource: Subject<string>;
    showTheme$: Observable<string>;
    change(theme: string): void;
}
