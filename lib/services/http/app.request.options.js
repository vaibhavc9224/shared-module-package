"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("@angular/http");
var index_1 = require("../../index");
//import {  Config  } from '../config/env.config';
var AppRequestOptions = /** @class */ (function (_super) {
    __extends(AppRequestOptions, _super);
    function AppRequestOptions() {
        return _super.call(this) || this;
    }
    AppRequestOptions.prototype.merge = function (options) {
        /* if(options !== null && options.headers !== null) {
         var url =  AppSettings.API_ENDPOINT + options.url;
         }*/
        if (options === null) {
            options = new http_1.RequestOptions();
        }
        options.headers = new http_1.Headers();
        options.headers.append('Content-Type', 'application/json');
        options.headers.append('Cache-Control', 'no-cache');
        options.headers.append('Pragma', 'no-cache'); //'If-Modified-Since'
        //options.headers.append('If-Modified-Since','Mon, 26 Jul 1997 05:00:00 GMT');
        options.headers.append('Authorization', 'Bearer ' + index_1.LocalStorageService.getLocalValue(index_1.LocalStorage.ACCESS_TOKEN));
        //options.url = `${Config.API}/` + options.url;
        options.url = index_1.AppSettings.API_ENDPOINT + options.url;
        var result = _super.prototype.merge.call(this, options);
        result.merge = this.merge;
        return result;
    };
    return AppRequestOptions;
}(http_1.RequestOptions));
exports.AppRequestOptions = AppRequestOptions;
//# sourceMappingURL=app.request.options.js.map