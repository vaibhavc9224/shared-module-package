"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Observable_1 = require("rxjs/Observable");
var index_1 = require("../../index");
var BaseService = /** @class */ (function () {
    function BaseService() {
    }
    BaseService.prototype.extractData = function (res) {
        var body = res.json();
        if (body.hasOwnProperty('access_token')) {
            index_1.LocalStorageService.setLocalValue(index_1.LocalStorage.ACCESS_TOKEN, body.access_token);
            if (body.data._id && body.data._id !== undefined) {
                index_1.LocalStorageService.setLocalValue(index_1.LocalStorage.USER_ID, body.data._id);
            }
        }
        return body || {};
    };
    BaseService.prototype.extractDataWithoutToken = function (res) {
        var body = res.json();
        return body || {};
    };
    BaseService.prototype.handleError = function (error) {
        return Observable_1.Observable.throw(error);
    };
    return BaseService;
}());
exports.BaseService = BaseService;
//# sourceMappingURL=base.service.js.map