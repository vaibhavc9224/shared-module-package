import { Response } from "@angular/http";
export declare class BaseService {
    extractData(res: Response): any;
    extractDataWithoutToken(res: Response): any;
    handleError(error: any): any;
}
