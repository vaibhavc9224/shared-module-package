import { RequestOptions, RequestOptionsArgs } from "@angular/http";
export declare class AppRequestOptions extends RequestOptions {
    constructor();
    merge(options?: RequestOptionsArgs): RequestOptions;
}
