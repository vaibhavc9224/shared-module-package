import { Observable } from "rxjs/Rx";
import { ConnectionBackend, Http, Request, RequestOptions, RequestOptionsArgs, Response } from "@angular/http";
export declare class CustomHttp extends Http {
    someProperty: string;
    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions);
    request(url: string | Request, options?: RequestOptionsArgs): Observable<any>;
    get(url: string, options?: RequestOptionsArgs): Observable<any>;
    post(url: string, body: any, options?: RequestOptionsArgs): Observable<any>;
    put(url: string, body: string, options?: RequestOptionsArgs): Observable<any>;
    delete(url: string, options?: RequestOptionsArgs): Observable<any>;
    intercept(observable: Observable<any>): Observable<Response>;
}
