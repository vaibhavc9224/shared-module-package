"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Rx_1 = require("rxjs/Rx");
var http_1 = require("@angular/http");
var index_1 = require("../../index");
var CustomHttp = /** @class */ (function (_super) {
    __extends(CustomHttp, _super);
    function CustomHttp(backend, defaultOptions) {
        var _this = _super.call(this, backend, defaultOptions) || this;
        _this.someProperty = 'some string';
        return _this;
    }
    CustomHttp.prototype.request = function (url, options) {
        return this.intercept(_super.prototype.request.call(this, url, options));
    };
    CustomHttp.prototype.get = function (url, options) {
        return this.intercept(_super.prototype.get.call(this, url, options));
    };
    CustomHttp.prototype.post = function (url, body, options) {
        return this.intercept(_super.prototype.post.call(this, url, body, options));
    };
    CustomHttp.prototype.put = function (url, body, options) {
        return this.intercept(_super.prototype.put.call(this, url, body, options));
    };
    CustomHttp.prototype.delete = function (url, options) {
        return this.intercept(_super.prototype.delete.call(this, url, options));
    };
    CustomHttp.prototype.intercept = function (observable) {
        // this.loaderService.start();
        return observable.do(function () { return console.log(''); })
            .catch(function (err, source) {
            //        this.loaderService.stop();
            var message = new index_1.Message();
            message.isError = true;
            var errorInstance = new index_1.ErrorInstance();
            if (err.err_msg && err.err_code) {
                errorInstance.err_msg = err.err_msg;
                errorInstance.err_code = err.err_code;
                return Rx_1.Observable.throw(errorInstance);
            }
            else if (err.status) {
                if (err.status === 401 || err.status === 403) {
                    errorInstance.err_code = err.status;
                    errorInstance.err_msg = JSON.parse(err._body).error.message;
                }
                else if (err.status === 404) {
                    errorInstance.err_msg = index_1.Messages.MSG_ERROR_SERVER_ERROR;
                    errorInstance.err_code = err.status;
                }
                else if (err.status === 0) {
                    errorInstance.err_msg = index_1.Messages.MSG_ERROR_SOMETHING_WRONG;
                    errorInstance.err_code = err.status;
                }
                else {
                    errorInstance.err_msg = JSON.parse(err._body).error.message;
                }
                return Rx_1.Observable.throw(errorInstance);
            }
            else {
                errorInstance.err_msg = index_1.Messages.MSG_ERROR_SOMETHING_WRONG;
                errorInstance.err_code = err.status;
                return Rx_1.Observable.throw(errorInstance);
            }
        });
    };
    CustomHttp = __decorate([
        core_1.Injectable()
    ], CustomHttp);
    return CustomHttp;
}(http_1.Http));
exports.CustomHttp = CustomHttp;
//# sourceMappingURL=custom.http.js.map