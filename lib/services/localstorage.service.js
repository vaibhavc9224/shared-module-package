"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LocalStorageService = /** @class */ (function () {
    function LocalStorageService() {
    }
    LocalStorageService.getLocalValue = function (key) {
        return localStorage.getItem(key);
    };
    LocalStorageService.removeLocalValue = function (key) {
        localStorage.removeItem(key);
    };
    LocalStorageService.setLocalValue = function (key, value) {
        localStorage.setItem(key, value);
    };
    LocalStorageService.ACCESS_TOKEN = 'access_token';
    return LocalStorageService;
}());
exports.LocalStorageService = LocalStorageService;
//# sourceMappingURL=localstorage.service.js.map