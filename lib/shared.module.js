"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var loader_component_1 = require("./loader/loader.component");
var loaders_service_1 = require("./loader/loaders.service");
var googleplace_directive_1 = require("./location/googleplace.directive");
var controlmessages_component_1 = require("./customvalidations/controlmessages.component");
var localstorage_service_1 = require("./services/localstorage.service");
var message_service_1 = require("./services/message.service");
var shared_service_1 = require("./services/shared-service");
var themechange_service_1 = require("./services/themechange.service");
var common_service_1 = require("./services/common.service");
var base_service_1 = require("./services/http/base.service");
var custom_http_1 = require("./services/http/custom.http");
var tool_tip_component_1 = require("./tool-tip-component/tool-tip-component");
var http_1 = require("@angular/http");
var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule
            ],
            declarations: [
                loader_component_1.LoaderComponent,
                controlmessages_component_1.ControlMessagesComponent,
                googleplace_directive_1.MyGoogleDirective,
                tool_tip_component_1.TooltipComponent
            ],
            exports: [
                loader_component_1.LoaderComponent,
                controlmessages_component_1.ControlMessagesComponent,
                googleplace_directive_1.MyGoogleDirective,
                tool_tip_component_1.TooltipComponent
            ],
            providers: [
                {
                    provide: http_1.Http,
                    useFactory: function (backend, defaultOptions) { return new custom_http_1.CustomHttp(backend, defaultOptions); },
                    deps: [http_1.XHRBackend, http_1.RequestOptions]
                },
                loaders_service_1.LoaderService,
                localstorage_service_1.LocalStorageService,
                message_service_1.MessageService,
                shared_service_1.SharedService,
                themechange_service_1.ThemeChangeService,
                common_service_1.CommonService,
                base_service_1.BaseService
            ]
        })
    ], SharedModule);
    return SharedModule;
}());
exports.SharedModule = SharedModule;
//# sourceMappingURL=shared.module.js.map